CLASSPATH="${CATALINA_BASE}/lib2/*"

CATALINA_OPTS="$CATALINA_OPTS \
 -server \
 -Djava.awt.headless=true \
 -Djava.security.egd=file:/dev/./urandom \
 -Dfile.encoding=UTF-8 \
 -Dsun.jnu.encoding=UTF-8 \
 -Duser.timezone=Europe/Berlin \
 -XX:+UseContainerSupport \
 -Xms2048M \
 -Xmx4096M \
 -XX:+UseG1GC \
 -XX:+UseStringDeduplication \
 -XX:G1ReservePercent=10 \
 -XX:ThreadStackSize=512k \
 -XX:+ExitOnOutOfMemoryError \
 -XshowSettings:vm"

CATALINA_OPTS="$CATALINA_OPTS \
 -Dmagnolia.bootstrap.authorInstance=false \
 -Dmagnolia.develop=false \
 -Dmagnolia.update.auto=true \
 -Dmagnolia.ui.sticker.color=blue \
 -Dmagnolia.home=/mgnl-home \
 -Dmagnolia.repositories.jackrabbit.config=\$\{magnolia.home\}/jackrabbit.xml \
 -Dmagnolia.repositories.home=\$\{magnolia.home\}/repositories \
 -Dmagnolia.resources.dir=\$\{magnolia.home\}/modules \
 -Dmagnolia.superuser.enabled=true \
 -Dmagnolia.bootstrap.license.owner=magnolia-demo@prod.magnolia-cloud.com \
 -Dmagnolia.bootstrap.license.key=LS0tLS1CRUdJTiBQR1AgTUVTU0FHRS0tLS0tClZlcnNpb246IEJDUEcgdjEuNTgKCm93SjRuRzFTVFdnVFFSaE50cmJxRmtFTnhRb2kyMVlQTGJ2cmJLcUdGaU1XclZnUGl1akpDaktabld5SDdNd3MKTTdQcEgrUlEwSlRpUVpCS2xlTEJrejFFRVRVb2dsancxSHFwRncrS0Z6MTRxUWplS29pVEJLTUZiOSs4eDN2ZgptemR6YzBlTFlTVFBWWjlWdXE0bm91VHE4dkZjT3cxWUdCS0VtY1N3NDI3VWMxb1FheWdPTEsvZkFtQVFwQWZUCmg2MlR3eGN2V1dtUTlzd2lGcEp3SnJPalIxM1BCWGJHQlM0WU5mazR3eUpMWWNCNFNLRGpZOHBQUklMN2JoTkMKSVk5OUYzRnE0b21JQ0tpMFM3Wm02WUFCeDB1YmxQdHhpR1VXTTRWRkpJakVka2lLR1BGbzBsWjRRbWtES0NYSgpFMVNYMmpEbktDd1ZZWUd0V1N3WURKMDhGMVRha29RNlpjUWpXOFlCRkVoUVcwSnRyVm1FYlVqaEZHZU83SGNRClp3d2p4WVhUMkcwanlCUjNVRXpqTUpZTzBVR0NSazU3SE9ka05JWUYxaUpLc2ZiWlJQOEJGZWZoWm1HQk0xaUEKUXYxWFoyS2ZxSHFaLzF6Njcram9Bdk5ZMXVxR1lYUEgxUWlpZ3UxRDJoaWc1aVlWUWJKeDVKRWlsRXpWN1JzSQppcVhpV3VsREJldUlLWWt1THVzQkFNelp4UzFHMGtpMHRScTFwMCtZMjNjMi84YjdsbDlkS1dOaGZ3YU5iVXVjCjMvTnBMdjl6dVhUdnd2cVBONkk0dlZLOS9mRHBsOTBMeDhxWjlsT2Q4K1U3KzI2ZEdVb2R1dGEyOUdCdWVxQjcKNVBHcnR6T0xQUy9PcnM3R1Q4b2R1NjZVUnZvZUxSMEV6eXZkSC9uTDRiWHFUT3RHMEx2eEx0emEyL2NWMzA5Ywp0am9ybjc4WjMxZHVyTDEyU3ViOGtROTdVOTZCOWQ4SitnVUcKPXlVMm8KLS0tLS1FTkQgUEdQIE1FU1NBR0UtLS0tLQo="


CATALINA_OPTS="$CATALINA_OPTS $CATALINA_OPTS_EXTRA"
