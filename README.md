# Magnolia Cloud Docker Compose Sample

Use this setup as a blueprint for running your Magnolia Cloud setup locally.

1. Build the war file

       mvn clean package
2. Run docker compose
       
       cd docker-compose
       docker-compose up

The command starts Author and Public apps with their corresponding PostgreSQL databases.<br>The repository and db files will be created in the respective *volumes* folders.<br><br>To test **light modules**, just copy your light-module-folder to *volumes/mgnl-author/modules* and *volumes/mgnl-public/modules* after the apps have started.

If you want to use Magnolia CMS DX Core features, please update the variables *magnolia.bootstrap.license.owner* and *magnolia.bootstrap.license.key* in the setenv.sh scripts with your license data.

Author-URL: http://localhost:8080/author <br>
Public-URL: http://localhost:8081/